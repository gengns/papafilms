import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const FILMS_API = 'http://www.omdbapi.com/?apikey=57a619cc&type=movie'

export default new Vuex.Store({
  plugins: [
    createPersistedState({
      paths: ['favorites'],
    }),
  ],

  state: { // = data
    films: [],
    favorites: [],
    info: {}
  },


  actions: { // = methods
    // Search films
    fetch_films({ commit, state }, text) {

      if (!text) return commit('set_films', [])

      return new Promise((resolve, reject) => {

        fetch(`${FILMS_API}&s=${text}`)
        .then(response => response.json())
        .then(({ Search:films = [], Error: error }) => {
          if (error) return reject(error)

          const fav_object = state.favorites
            .map(f => f.imdbID)
            .reduce((acc, e)=> (acc[e] = true, acc), {})
          
          // Add class fav (want to show in search results)
          films = films.map(film => {
            film.is_fav = !!fav_object[film.imdbID]
            return film
          })

          commit('set_films', films)
          resolve()
        })
        .catch(error => reject(error))
      })

    },

    // Toggle fav
    toggle_fav_film({ commit, state }, film) {
      const index = 
        state.favorites.findIndex(f => f.imdbID == film.imdbID)

      if (index < 0) {
        film.is_fav = true
        commit('add_favorite_film', film)
      } else {
        film.is_fav = false
        commit('remove_favorite_film', index)
      }
    },

    // Fetch info
    fetch_info({ commit }, imdbID) {
      return new Promise((resolve, reject) => {

        fetch(`${FILMS_API}&i=${imdbID}`)
        .then(response => response.json())
        .then(data => {
          if (!data) return reject()
          if (data.Error) return reject(data.Error)

          commit('set_info', data)
          resolve()
        })
        .catch(error => reject(error))
      })
    }
  },


  mutations: { // = update the state
    set_films(state, films) {
      state.films = films
    },
    add_favorite_film(state, film) {
      state.favorites.push(film)
    },
    remove_favorite_film(state, index) {
      state.favorites.splice(index, 1)
    },
    set_info(state, info) {
      state.info = info
    }
  }
})
