import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.config.ignoredElements = [
  'g-search',
  'g-title',
  'icon-search',
  'icon-star',
  'g-favorites',
  'g-info',
  'g-films'
]

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
