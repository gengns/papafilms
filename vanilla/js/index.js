/**
  @file index.js

  @author Génesis García Morilla <gengns@gmail.com>
  @date 2020-5

  @description Main
*/

// Elements
const $bar = $('g-progress-bar')
const $search = $('nav > a:first-child')
const $favorites = $('nav > a:last-child')
const $input = $('input')
const $films = $('g-films')


// Toggle favorite
function toogle_fav(event, $figure) {
  const imdbID = $figure.id
  const Title = $figure.querySelector('figcaption').textContent
  const Poster = $figure.querySelector('img').src
  let fav_films = JSON.parse(localStorage.fav_films)

  // Any part of the figure toggle favorite except figcaption
  if (event.target.localName == 'figcaption') 
    return open(`https://imdb.com/title/${imdbID}`, '_blank')

  $figure.classList.toggle('fav')

  // Add film
  if ($figure.className) fav_films.push({ Title, Poster, imdbID })
  // Remove film
  else {
    fav_films.splice(fav_films.findIndex(f => f.imdbID == imdbID), 1)
    // If we are in favorites
    if ($favorites.className) $figure.remove()
  }

  // Save favorites
  localStorage.fav_films = JSON.stringify(fav_films)
}


// Get a list of films in HTML
function get_html_films(films, favs) {
  return films.reduce((acc, { Poster, Title, imdbID }) => {
    // Show as favorite if saved in favorites
    const className = 
      !favs ? 'fav' : favs.some(f => f.imdbID == imdbID) ? 'fav' : ''

    acc +=
    `<figure id="${imdbID}" class="${className}" onclick="toogle_fav(event, this)">
      <icon-star></icon-star>
      <img src="${Poster}" loading="lazy" width="300" height="400">
      <figcaption>${Title}</figcaption>
    </figure>`

    return acc
  }, '')
}


// Switch between Search and Favorites
;[$search, $favorites].forEach($a => $a.onclick = () => {
  if ($a.className) return
  $search.classList.toggle('active')
  $favorites.classList.toggle('active')

  if ($a == $search) {
    $input.className = 'show'
    $films.innerHTML = ''
  } else {
    $input.value = ''
    $input.className = ''
    // Set favorite films
    $films.innerHTML = get_html_films(JSON.parse(localStorage.fav_films))
  }
})


// Seach film
$input.onkeyup = e => {
  if (e.key != 'Enter') return
  $bar.classList.add('show')

  fetch(`${FILMS_API}&s=${$input.value}`)
    .then(response => response.json())
    .then(({ Search = [], Error: error }) => {
      $input.blur()
      if (error) return alert(error)

      // Set found films
      $films.innerHTML = 
        get_html_films(Search, JSON.parse(localStorage.fav_films))
    })
    .catch(error => alert(error))
    .finally(() => $bar.classList.remove('show'))
}