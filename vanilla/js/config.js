/**
  @file config.js

  @date 2020-5
  @author Génesis García Morilla <gengns@gmail.com>

  @description Basic configuration
*/


//==============================================================================
// GENERAL PARAMETERS
//==============================================================================
const FILMS_API = 'http://www.omdbapi.com/?apikey=57a619cc&type=movie'


//==============================================================================
// LOCAL STORAGE
//==============================================================================
localStorage.fav_films = localStorage.fav_films || JSON.stringify([])


//==============================================================================
// UTILITIES
//==============================================================================
/**
 * Short selectors like jQuery
 * @param {string} selector 
 */
const $ = selector => document.querySelector(selector)
const $$ = selector => document.querySelectorAll(selector)